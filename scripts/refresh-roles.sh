#!/bin/bash

# Run this from project root to refresh roles stored in upstream repos

# Remove existing project roles stored in upstream repos
rm -rf  roles/gitlab-runner \
        roles/git \
        roles/geerlingguy.docker

# Install project roles (refresh roles from upstream repos)
ansible-galaxy install -r roles/requirements.yml