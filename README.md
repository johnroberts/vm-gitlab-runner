# Summary
This repo implements a GitLab runner VM for Vagrant+Virtualbox, based on Ubuntu LTS (22.04) and Ansible.

Handy as a git submodule for projects that use GitLab CI, but shouldn't run on GitLab-managed runners (for cost/security/performance reasons).

# Usage
## Create .gitlab-ci.yml
Copy [.gitlab-ci.yml](.gitlab-ci.yml) to `.gitlab-ci.yml` at your project root, and adapt it as needed. Include:
- `tags: [local]` in each job to execute it on the GitLab runner VM (rather than on GitLab infra)
- Stages aligned with the project's design and technical maturity. Common stages: `build`, `test`, `deploy`

## Set up GitLab runner VM
Make sure your host has Vagrant, Virtualbox, and Ansible. Then:
```shell
# Clone repo
git clone https://gitlab.com/johnroberts/vm-gitlab-runner.git
cd vm-gitlab-runner

# Populate/refresh Ansible roles
scripts/refresh-roles.sh

# Run VM, open a shell to it
vagrant up && vagrant ssh
```

To execute CI jobs in the VM, you need to register it with a GitLab project. You'll need the GitLab instance's URL and registration token from the GitLab project you want to use this VM with. Go to the project's Settings -> CI/CD -> Runners, noting of the URL and registration token.

Note: you may also wish to disable shared runners.

```shell
# On the runner VM's console:
sudo gitlab-runner register

# Answer the prompts:
#   - Enter the URL and registration token
#   - Add the "local" tag (so tagged jobs in .gitlab-ci.yml execute here)
#   - Executor type: docker and shell are both solid choices. The runner VM sets up Docker and git.
#   - For help choosing an executor, see https://docs.gitlab.com/runner/executors/#i-am-not-sure)
#   - If you choose the docker executor, it will auto-suggest a sensible default like ruby:2.7
```
